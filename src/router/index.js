import Vue from 'vue'
import Router from 'vue-router'
import { customAlphabet } from 'nanoid'
const nanoid = customAlphabet('1234567890', 15)

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * 公共页面，没有权限要求
 */
export const constantRoutes = [
  {
    path: '/login',
    meta: {
      id: nanoid()
    },
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    meta: {
      id: nanoid()
    },
    component: () => import('@/views/404'),
    hidden: true
  },
  // 首页1
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    meta: {
      id: nanoid()
    },
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/home/index'),
        meta: { title: '首页', level: 0, id: '0' }
      }
    ]
  }
]
/**
 * 非公共页面，根据后端返回对应权限
 */
export const asyncRouterMap = [
  // 酿造
  // {
  //   path: '/brew',
  //   component: Layout,
  //   redirect: '/reportView',
  //   name: '酿造',
  //   meta: { title: '酿造', level: 0, id: '1' },
  //   alwaysShow: true,
  //   children: [
  //     {
  //       path: '/reportView/:id',
  //       name: 'reportView',
  //       component: () => import('@/components/PowerBI/index'),
  //       meta: { title: '报表1', id: '9900', level: 3 }
  //     },
  //     {
  //       path: '/reportView/:id',
  //       name: 'reportView',
  //       component: () => import('@/components/PowerBI/index'),
  //       meta: { title: '报表2', id: '99', level: 3 }
  //     }
  //   ]
  // },
  // 包装
  // {
  //   path: '/packing',
  //   component: Layout,
  //   redirect: '/reportView',
  //   name: 'packing',
  //   alwaysShow: true,
  //   meta: { title: '包装', level: 0, id: '2' },
  //   children: [
  //     {
  //       path: '/packing/reportView/:id',
  //       name: 'reportForm1',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: '报表1', icon: 'table', id: '299835454009413' }
  //     },
  //     {
  //       path: 'reportForm2',
  //       name: 'reportForm2',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: '报表·2', icon: 'tree' }
  //     }
  //   ]
  // },
  // 动力
  // {
  //   path: '/power',
  //   component: Layout,
  //   redirect: '/reportView',
  //   name: 'power',
  //   meta: { title: '动力', icon: 'el-icon-s-help', level: 1 },
  //   children: [
  //     {
  //       path: '/reportView/:id',
  //       name: 'reportView',
  //       component: () => import('@/components/PowerBI/index'),
  //       meta: { title: 'XIX TS BI', id: '299835943448645', level: 3 }
  //     }
  //   ]
  // },
  // 系统设置
  // {
  //   path: '/system',
  //   component: Layout,
  //   redirect: '/setting/user',
  //   name: 'system',
  //   meta: { title: '设置', icon: 'el-icon-s-help', level: 1 },
  //   children: [
  //     {
  //       path: '/setting/user',
  //       name: 'user',
  //       component: () => import('@/views/setting/index'),
  //       meta: { title: '用户管理', icon: 'table' }
  //     },
  //     {
  //       path: '/setting/role',
  //       name: 'role',
  //       component: () => import('@/views/setting/index'),
  //       meta: { title: '角色管理', icon: 'tree' }
  //     }
  //   ]
  // }
  // { path: '*', redirect: '/404', hidden: true }
]

export const redirectRouter = [
  {
    path: '*',
    redirect: '/404',
    hidden: true,
    meta: {
      id: nanoid()
    }
  }
]

const createRouter = () =>
  new Router({
    mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
