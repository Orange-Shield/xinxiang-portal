import request from '@/utils/request'

export function getSingleReport(params) {
  return request({
    url: '/api/GetCompleteReportInfo',
    method: 'get',
    params
  })
}
export function getPowerBiInfoByMenuId(params) {
  return request({
    url: '/api/sysMenu/getPowerBiInfoByMenuId',
    method: 'get',
    params
  })
}
// 创建或更新ReportBookMark
export function setReportBookMark(data) {
  return request({
    url: '/api/reportBookMark/createOrUpdate',
    method: 'post',
    data
  })
}
// 创建或更新ReportBookMark
export function getReportBookMark(params) {
  return request({
    url: '/api/reportBookMark/getBookMarksByReportInGroup',
    method: 'get',
    params
  })
}
// 删除书签
export function deleteReportBookMark(params) {
  return request({
    url: '/api/reportBookMark/delete',
    method: 'delete',
    params
  })
}
// 设置默认书签书签
export function setDefaultBookmark(params) {
  return request({
    url: '/api/reportBookMark/default',
    method: 'get',
    params
  })
}
