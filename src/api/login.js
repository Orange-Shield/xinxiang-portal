import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/login',
    method: 'post',
    data
  })
}

export function adLogin(data) {
  return request({
    url: '/api/ADLogin',
    method: 'post',
    data
  })
}

// 从后端获取AAD登录地址
export function aadLoginUrl(params) {
  return request({
    url: '/api/aadLoginUrl',
    method: 'get',
    params
  })
}

// 重定向AAD登陆地址
export function aadLogin(params) {
  return request({
    url: '/api/aadLogin',
    method: 'post',
    params
  })
}

export function getLoginUser() {
  return request({
    url: '/api/getLoginUser',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/api/logout',
    method: 'get'
  })
}

export function getAsyncRoutes(params) {
  return request({
    url: '/api/sysMenu/usertree',
    method: 'get',
    params
  })
}
