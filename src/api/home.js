import request from '@/utils/request'

// 获取收藏列表
export function getColectionList(params) {
  return request({
    url: '/api/getColectionList',
    method: 'post',
    params
  })
}

// 添加收藏
export function addCollection(data) {
  return request({
    url: '/api/addCollection',
    method: 'post',
    data
  })
}

// 更新收藏
export function updateCollection(data) {
  return request({
    url: '/api/updateCollection',
    method: 'post',
    data
  })
}

// 删除收藏
export function deleteCollection(data) {
  return request({
    url: '/api/deleteCollection',
    method: 'post',
    data
  })
}

// 获取收藏的默认图标
export function getIcons() {
  return request({
    url: '/api/collection/icon',
    method: 'get'
  })
}

// 获取收藏的默认图标
export function deleteIcons(params) {
  return request({
    url: '/api/collection/icon/' + params,
    method: 'delete'
  })
}

// 获取用户权限内报表
export function userReports() {
  return request({
    url: '/api/userpage/userreports',
    method: 'get'
  })
}

// 获取页签
export function getAzureReportPages(params) {
  return request({
    url: '/api/GetAzureReportPages',
    method: 'get',
    params
  })
}

// 更新我的桌面配置(有配置的时候)
export function userDesktop(data) {
  return request({
    url: '/api/userpage/userdesktop',
    method: 'post',
    data
  })
}
// 更新我的桌面配置(无配置的时候)
export function initUserPage(params) {
  return request({
    url: '/api/userpage/InitUserPage',
    method: 'get',
    params
  })
}

// 获取多桌面标签
export function userDesktopPagelist() {
  return request({
    url: '/api/userpage/userdesktopPagelist',
    method: 'get'
  })
}

// 根据桌面Id获得对应的桌面配置
export function userDesktopListByPageId(params) {
  return request({
    url: '/api/userpage/userdesktoplistByPageId',
    method: 'get',
    params
  })
}

// 更新我的桌面标签
export function userDesktopPage(data) {
  return request({
    url: '/api/userpage/userdesktopPage',
    method: 'post',
    data
  })
}

// 删除我的桌面标签
export function deleteUserDesktopPage(params) {
  return request({
    url: '/api/userpage/deleteUserDesktopPage',
    method: 'get',
    params
  })
}

// 获取模板详情
export function getPageTemplate(params) {
  return request({
    url: '/api/userPageTemplate/' + params,
    method: 'get'
  })
}

// 获取模板列表
export function getPageTemplateList() {
  return request({
    url: '/api/userPageTemplateList',
    method: 'get'
  })
}
