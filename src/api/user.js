import request from '@/utils/request'
// 获取用户列表
export function getUserPage(params) {
  return request({
    url: '/api/sysUser/page',
    method: 'get',
    params
  })
}
// 根据Id获取用户信息
export function getUserInfoById(params) {
  return request({
    url: '/api/sysUser/getUserInfoById',
    method: 'get',
    params
  })
}
// 新增用户
export function addUser(data) {
  return request({
    url: '/api/sysUser/add',
    method: 'post',
    data
  })
}
// 删除用户
export function deleteUser(data) {
  return request({
    url: '/api/sysUser/delete/users',
    method: 'post',
    data
  })
}
// 更新用户信息
export function updateUser(data) {
  return request({
    url: '/api/sysUser/update',
    method: 'put',
    data
  })
}
// 更新用户状态
export function updateUserStatus(data) {
  return request({
    url: '/api/sysUser/status',
    method: 'post',
    data
  })
}
// 重置用户默认密码
export function resetUserPassword(data) {
  return request({
    url: '/api/sysUser/password/reset',
    method: 'post',
    data
  })
}

// 更新用户密码
export function updateUserPassword(data) {
  return request({
    url: '/api/sysUser/password',
    method: 'post',
    data
  })
}
// 授权用户角色
export function grantUserRole(data) {
  return request({
    url: '/api/sysUser/grantRole',
    method: 'post',
    data
  })
}
// 授权用户范围
export function grantUserData(data) {
  return request({
    url: '/api/sysUser/grantData',
    method: 'post',
    data
  })
}
// 获取用户角色
export function getUserRole(params) {
  return request({
    url: '/api/sysUser/ownRole',
    method: 'get',
    params
  })
}
// 获取用户数据范围
export function getUserData(params) {
  return request({
    url: '/api/sysUser/ownData',
    method: 'get',
    params
  })
}
// 导出用户列表 excel
export function exportUserList() {
  return request({
    url: '/api/sysUser/export',
    method: 'get',
    responseType: 'blob'
  })
}
//  下载模板
export function getTemplate() {
  return request({
    url: '/api/sysUser/import/template',
    method: 'get',
    responseType: 'blob'
  })
}
// 批量导入用户
export function importUserList(data) {
  return request({
    url: '/api/sysUser/import',
    method: 'post',
    data: data
  })
}
// 获取角色列表
export function getRoleData(params) {
  return request({
    url: '/api/sysRole/dropDown',
    method: 'get',
    params
  })
}
// 获取组织机构树
export function getOrganizeTree() {
  return request({
    url: '/api/sysOrg/tree',
    method: 'get'
  })
}
