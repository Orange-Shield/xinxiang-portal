import request from '@/utils/request'
// 获取部门机构树
export function getOrganizeTree() {
  return request({
    url: '/api/sysOrg/tree',
    method: 'get'
  })
}
// 增加部门机构
export function addOrganizeTree(data) {
  return request({
    url: '/api/sysOrg',
    method: 'post',
    data
  })
}
// 删除部门机构
export function deleteOrganizeTree(id) {
  return request({
    url: '/api/sysOrg/' + id,
    method: 'delete'
  })
}
// 更新部门机构
export function updateOrganizeTree(data) {
  return request({
    url: '/api/sysOrg',
    method: 'put',
    data
  })
}
