import request from '@/utils/request'

// 获取访问日志列表
export function getLogVisPage(params) {
  return request({
    url: '/api/logVis/page',
    method: 'get',
    params
  })
}
