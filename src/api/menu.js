import request from '@/utils/request'

// 获取菜单
export function getUserTree(params) {
  return request({
    url: '/api/sysMenu/usertree',
    method: 'get',
    params
  })
}
// 添加菜单
export function addMenu(data) {
  return request({
    url: '/api/sysMenu/add',
    method: 'post',
    data
  })
}
// 删除菜单
export function deleteMenu(data) {
  return request({
    url: '/api/sysMenu/delete',
    method: 'post',
    data
  })
}
// 编辑菜单
export function editMenu(data) {
  return request({
    url: '/api/sysMenu/edit',
    method: 'post',
    data
  })
}
// 获取菜单树结构
export function getMenuTreeForGrant(params) {
  return request({
    url: '/api/sysMenu/treeForGrant',
    method: 'get',
    params
  })
}
// 获取PowerBi应用
export function getAllAzureAuthApp() {
  return request({
    url: '/api/GetAllAzureAuthApp',
    method: 'get'
  })
}
// 获取PowerBI工作区
export function getPtlPbiGroupByAuth(params) {
  return request({
    url: '/api/GetPtlPbiGroupByAuth',
    method: 'get',
    params
  })
}
// 获取PowerBI报表
export function getReportsInGroup(params) {
  return request({
    url: '/api/GetReportsInGroup',
    method: 'get',
    params
  })
}
