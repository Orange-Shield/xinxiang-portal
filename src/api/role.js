import request from '@/utils/request'

// 获取角色-分页
export function getRolePage(params) {
  return request({
    url: '/api/sysRole/page',
    method: 'get',
    params
  })
}
// 新增角色
export function addRole(data) {
  return request({
    url: '/api/sysRole',
    method: 'post',
    data
  })
}
// 更新角色
export function updateRole(data) {
  return request({
    url: '/api/sysRole',
    method: 'put',
    data
  })
}
// 删除角色
export function deleteRole(data) {
  return request({
    url: '/api/sysRole',
    method: 'delete',
    data
  })
}
// 获取角色拥有菜单Id集合
export function getMenuByRoleId(params) {
  return request({
    url: '/api/sysRole/menu',
    method: 'get',
    params
  })
}
// 授权角色菜单
export function authorizRoleMenu(data) {
  return request({
    url: '/api/sysRole/menu',
    method: 'POST',
    data
  })
}
// 授权角色数据范围
export function authorizRoleData(data) {
  return request({
    url: '/api/sysRole/data',
    method: 'post',
    data
  })
}
