/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @param {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * Form 表单验证
 */
/**
 * 表单 密码验证
 * @param {rule} 表单规则
 * @returns {value} 表单值
 * @returns {callback} 回调函数
 * @returns {form} 表单实例 this.$form
 */
export function password(rule, value, callback, form) {
  const reg = new RegExp(/^[a-zA-Z]\w{5,17}$/)
  const { rePassword } = form.model
  if (!reg.test(value)) {
    callback(
      new Error(
        '密码应以字母开头，长度为6~18个字符，且只能包含字母、数字和下划线！'
      )
    )
  } else if (rePassword !== '' && rePassword !== undefined) {
    form.validateField('rePassword')
    callback()
  }
}
/**
 * 表单 重复密码验证
 * @param {rule} 表单规则
 * @returns {value} 表单值
 * @returns {callback} 回调函数
 * @returns {form} 表单实例 this.$form
 */
export function rePassword(rule, value, callback, form) {
  console.log(value)
  const { password } = form.model
  // if (password === '' || password === undefined) {
  //   console.log(value)
  //   callback()
  // } else
  if (password !== value) {
    console.log(value)
    callback(new Error('请确认两次输入密码的一致性！'))
  } else {
    callback()
  }
}
/**
 * 表单 手机号验证
 * @param {rule} 表单规则
 * @returns {value} 表单值
 * @returns {callback} 回调函数
 */
export function phone(rule, value, callback) {
  const reg = new RegExp(/^1[3-9]\d{9}$/)
  if (value !== '' && value !== undefined && !reg.test(value)) {
    callback(new Error('请输入正确的手机号！'))
  }
  callback()
}
