import Cookies from 'js-cookie'

const Bearer = 'Bearer '
const TokenKey = 'token'
const RefreshToken = 'refresh_token'

// cookie获取token
export function getToken() {
  return Cookies.get(TokenKey)
}
// cookie添加token
export function setToken(token) {
  return Cookies.set(TokenKey, Bearer + token)
}
// cookie移除token
export function removeToken() {
  return Cookies.remove(TokenKey)
}
// cookie添加refreshToken
export function setRefreshToken(refreshToken) {
  // 空置时间  到时间则退出到登陆界面 分钟
  const expiresTime = 30
  const curMillsecond = new Date().getTime()
  const expires = new Date(curMillsecond + 60 * 1000 * expiresTime)
  return Cookies.set(
    RefreshToken,
    refreshToken.indexOf(Bearer) === 0 ? refreshToken : Bearer + refreshToken,
    {
      expires: expires
    }
  )
}
// cookie获取refreshToken
export function getRefreshToken() {
  return Cookies.get(RefreshToken)
}
// cookie移除refreshToken
export function removeRefreshToken() {
  return Cookies.remove(RefreshToken)
}
// cookie重置refreshToken
export function resetRefreshToken() {
  const refreshToken = getRefreshToken()
  if (refreshToken !== undefined) {
    removeRefreshToken()
    setRefreshToken(refreshToken)
  }
}
