import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import {
  getRefreshToken,
  getToken,
  resetRefreshToken,
  setRefreshToken,
  setToken
} from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 30 * 1000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    // 每次发起请求重置refreshToken
    resetRefreshToken()
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['Authorization'] = getToken()
      config.headers['X-Authorization'] = getRefreshToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    // 判断header中是否包含token 和 refreshToken
    if (response.headers['access-token']) {
      setToken(response.headers['access-token'])
    }
    if (response.headers['x-access-token']) {
      setRefreshToken(response.headers['x-access-token'])
    }
    // 判断是否为Blob类型数据 是则直接返回response
    if (response.request.responseType === 'blob') {
      return response
    }
    const res = response.data
    // 拦截白名单
    const whiteList = [200, 204]
    if (whiteList.indexOf(res.code) === -1) {
      Message({
        showClose: true,
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000 // 3s 弹窗显示时间
      })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 401) {
        // to re-login
        MessageBox.confirm('登录已过期，请重新登录！', '警告', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      showClose: true,
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
