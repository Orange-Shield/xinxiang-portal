import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      const hasGetUserInfo = store.getters.role
      if (Object.keys(hasGetUserInfo).length !== 0) {
        next()
      } else {
        try {
          // get user info
          const role = await store.dispatch('user/getInfo')
          const allRoutes = await store.dispatch('user/getAsyncRoutes')
          router.addRoutes(allRoutes)
          router.options.routes = allRoutes
          if (!('accountId' in to.query)) {
            //  如果没有url没有记录退出的id  正常跳转
            next({ ...to, replace: true })
          } else if (to.query.accountId === role.id.toString()) {
            //  如果url记录的上一个退出id和当前的角色id相同，正常跳转
            next({ path: to.path, replace: true })
          } else {
            //  如果url记录的上一个退出id和当前id不同，为了避免无权限问题，重定向到首页。
            next({ path: '/' })
          }
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message({
            showClose: true,
            message: error || 'Has Error',
            type: 'error',
            duration: 5 * 1000
          })
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
