import * as Api from '@/api/login'
import {
  getToken,
  setToken,
  removeToken,
  removeRefreshToken
} from '@/utils/auth'
import { resetRouter, constantRoutes, redirectRouter } from '@/router'
import Layout from '@/layout'

const getDefaultState = () => {
  return {
    token: getToken(),
    role: {},
    routes: []
  }
}

// 根据后端返回数据生成路由
const generateRoute = routes => {
  const asyncRoutes = []
  if (Object.keys(routes).length !== 0) {
    // 如果routes不为空，即用户有权限时，遍历并生成路由。
    routes.map(item => {
      // 对应的前端组件。
      if (item.type !== 0) {
        item.component = require('@/views/' + item.component).default
      } else {
        item.component = Layout
      }

      // 遍历子项
      if (item.children.length !== 0) {
        const children = generateRoute(item.children)
        item.children = children
      }
      // 过滤设置为不可见的菜单。
      if (item.visible) {
        asyncRoutes.push({
          path: item.router,
          component: item.component,
          name: item.name,
          alwaysShow: item.type === 0,
          meta: {
            title: item.name,
            level: item.type,
            id: item.idStr
          },
          children: item.children
        })
      }
    })
  }
  return asyncRoutes
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: state => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_ROLE: (state, role) => {
    state.role = role
  },
  SET_ROUTES: (state, routes) => {
    console.log(routes)
    state.routes = routes
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { account, password, loginType } = userInfo
    const loginApi = loginType === 0 ? 'login' : 'adLogin'
    return new Promise((resolve, reject) => {
      Api[loginApi]({ account: account.trim(), password: password })
        .then(res => {
          const token = res.data
          commit('SET_TOKEN', token)
          setToken(token)
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // user aad login
  aadLogin({ commit }, code) {
    return new Promise((resolve, reject) => {
      const params = {
        code: code
      }
      Api.aadLogin(params)
        .then(res => {
          const token = res.data
          commit('SET_TOKEN', token)
          setToken(token)
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      Api.getLoginUser(state.token)
        .then(response => {
          const { data } = response

          if (!data) {
            return reject('Verification failed, please Login again.')
          }

          commit('SET_ROLE', data)
          resolve(data)
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      Api.logout(state.token)
        .then(() => {
          removeToken() // must remove  token  first
          removeRefreshToken()
          resetRouter()
          commit('RESET_STATE')
          resolve()
        })
        .catch(error => {
          reject(error)
        })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  },

  // get asyncRoutes
  getAsyncRoutes({ commit }) {
    return new Promise((resolve, reject) => {
      const params = {
        appCode: 'Supply_Portal'
      }
      Api.getAsyncRoutes(params)
        .then(res => {
          const asyncRoutes = generateRoute(res.data)
          const allRoutes = [
            ...constantRoutes,
            ...asyncRoutes,
            ...redirectRouter
          ]
          commit('SET_ROUTES', allRoutes)
          resolve(allRoutes)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
